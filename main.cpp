/*
  Short job 2
*/
#include <cstdlib>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
    double re, im;
} complex_t;

int vyprazdni(int *temp, int k) {
    // flush cache
    int i, s = 0;
    for (i = 0; i < k; i++)
        s += temp[i];
    return s;
}

int testik(complex_t *c, int n) {
    double re, im, r2, im2;
    int i2, j2, k2;
    int nn = 0;
    re = im = 0.0;
    for (j2 = 0; j2 < n; j2++) {
        for (i2 = 0; i2 < n; i2++) {
            re += c[i2 * n + j2].re;
            im += c[i2 * n + j2].im;
        }
    }
    if (isnan(re))
        nn++;
    if (isnan(im))
        nn++;
    printf("%10.8g %10.8g ", re, im);
    k2 = 0;

    if (n == 832) {
        r2 = 48192;
        im2 = 297600;
    }
    if (n == 1152) {
        r2 = 67200;
        im2 = 489472;
    }
    if (n == 1472) {
        r2 = 10560;
        im2 = 1006336;
    }
    if (n == 1792) {
        r2 = 446464;
        im2 = 1742848;
    }
    if (n == 2112) {
        r2 = -91072;
        im2 = -884544;
    }
    double x = fabs(re - r2) + fabs(im - im2);
    if (x < 10)
        k2 = 1;
    if (nn)
        k2 = 0;
    printf("%g ", x);
    return k2;
}

// zacatek casti k modifikaci
// beginning of part for modification

void gemm(complex_t const *__restrict__ a, complex_t const *__restrict__ b,
          complex_t *__restrict__ c, const int n) {
    int i2, j2, k2;
    int BSZ = 64;
    float *aa_re = (float *)malloc(n * n * sizeof(float));
    float *aa_im = (float *)malloc(n * n * sizeof(float));
    float *bb_re = (float *)malloc(n * n * sizeof(float));
    float *bb_im = (float *)malloc(n * n * sizeof(float));

#pragma omp parallel for collapse(2)
    for (int r = 0; r < n; r++) {
        for (int c = 0; c < n; c++) {
            aa_im[r * n + c] = a[r * n + c].im;
            aa_re[r * n + c] = a[r * n + c].re;
            bb_re[c * n + r] = b[c * n + r].re;
            bb_im[c * n + r] = b[c * n + r].im;
        }
    }

    for (int a_row_it = 0; a_row_it < n; a_row_it++) {
        for (int a_row_block_it = 0; a_row_block_it < n; a_row_block_it += BSZ) {
#pragma omp parallel for
            for (int b_row_block_it = 0; b_row_block_it < n; b_row_block_it += BSZ) {
                for (int a_block_it = 0; a_block_it < BSZ; a_block_it++) {
                    for (int b_block_it = 0; b_block_it < BSZ; b_block_it++) {
                        c[a_block_it * n + b_row_block_it + b_block_it].im += aa_re[a_row_it * n + a_row_block_it + a_block_it] * bb_im[a_row_it * n + b_row_block_it + b_block_it] +
                                                                              aa_im[a_row_it * n + a_row_block_it + a_block_it] * bb_re[a_row_it * n + b_row_block_it + b_block_it];
                        c[a_block_it * n + b_row_block_it + b_block_it].re += aa_re[a_row_it * n + a_row_block_it + a_block_it] * bb_re[a_row_it * n + b_row_block_it + b_block_it] -
                                                                              aa_im[a_row_it * n + a_row_block_it + a_block_it] * bb_im[a_row_it * n + b_row_block_it + b_block_it];
                    }
                }
            }
        }
    }
}

// konec casti k modifikaci
// end of part for modification

int main(void) {

    double timea[60];

    int soucet = 0, N, i, j, k, ni, nj, nk, *pomo, v;
    int ri, rj, rk;
    double delta, s_delta = 0.0;
    complex_t *mA, *mB, *mC;

    int ti[4] = {3500, 5000, 3200, 2500};
    int tj[4] = {3500, 3200, 2500, 5000};
    int tk[4] = {3500, 2500, 5000, 3200};

    srand(time(NULL));
    pomo = (int *)malloc(32 * 1024 * 1024);
    v = 0;
    for (N = 0; N < 5; N++) {
        ni = 832 + 320 * N;
        nj = ni;
        nk = ni;
        printf("%i ", ni);
        mA = (complex_t *)malloc(ni * nk * sizeof(complex_t));
        mB = (complex_t *)malloc(nk * nj * sizeof(complex_t));
        mC = (complex_t *)malloc(ni * nj * sizeof(complex_t));

        for (i = 0; i < ni; i++) {
            // ri=rand();
            for (k = 0; k < nk; k++) {
                rk = (k ^ i) ^ ni;
                mA[i * nk + k].re = (rk % 13 - 6.0);
                mA[i * nk + k].im = (rk % 17 - 8.0);
            }
        }
        for (k = 0; k < nk; k++) {
            // rk=rand();

            for (j = 0; j < nj; j++) {
                rk = (k ^ j) ^ ni;
                mB[k * nj + j].re = (rk % 19 - 9.0);
                mB[k * nj + j].im = (rk % 11 - 5.0);
            }
        }
        for (i = 0; i < ni; i++) {
            // ri=rand();
            for (j = 0; j < nj; j++) {
                // rj=rand();
                mC[i * nj + j].re = 0.0;
                mC[i * nj + j].im = 0.0;
            }
        }

        double di;

        soucet += vyprazdni(pomo, v);
        timea[0] = omp_get_wtime();
        // improve performance of this call
        // vylepsit vykonnost tohoto volani
        gemm(mA, mB, mC, ni);
        timea[1] = omp_get_wtime();
        i = testik(mC, ni);
        delta = 10.0 * di * di * di / (timea[1] - timea[0]);
        s_delta += (timea[1] - timea[0]);
        // i=testuj(mA,mB,mC,mC2,ni,nj,nk,1.0,1.0);
        printf("%i %g ", i, (timea[1] - timea[0]));

        printf("\n");
        if (i == 0) {
            printf("Incorrect result !!!\n");
            return 0;
        }
        if (s_delta > 20.0) {
            printf(
                "Time limit (20 sec) is reached (time=%g s). SJ2 points: 0\n",
                s_delta);
            return 0;
        }

        free(mC);
        free(mB);
        free(mA);
    }
    float b = 0.526 / s_delta * 12.0;
    if (b > 15.0)
        b = 15.0;
    printf("Total time: %g\n", s_delta);
    printf("SJ2 points: %g\n", b);
    return 0;
}
