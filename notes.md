# Notes

- default
- `-march=native`
- `-ffast-math`
  - time limit reached after 2 tests

- parallel for + `march=native` + `-ffast-math`
  - Total time: 15.6167
  - SJ2 points: 0.404183

- parallel for + `march=native` + `-ffast-math` + `restrict`
  - Total time: 15.6455
  - SJ2 points: 0.403438

- parallel for + `march=native` + `-ffast-math` + `restrict` + swap loops
  - Total time: 14.7906
  - SJ2 points: 0.426757
