sj: main.cpp
	g++ -O3 -march=native -fopenmp -ffast-math -ftree-vectorize main.cpp -o sj # -fopt-info-vec -fopt-info-vec-missed -static

clean:
	rm -f sj run.* core.*

run: clean
	qrun 20c 1 pdp_fast run
